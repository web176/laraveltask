<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Auth\LoginController@showLoginForm');

Route::prefix('admin')->group(function ( ){
    Route::get('/', 'Auth\LoginController@showLoginForm');

    Route::get('/','Auth\LoginController@showLoginForm')->name('adminLogin');
    Route::get('/login','Auth\LoginController@showLoginForm')->name('adminLogin');
    Route::post('/login',[ 'as' => 'login', 'uses' => 'Auth\LoginController@login']);
    
    Route::middleware(['auth:web'])->group(function (){

        Route::get('logout', 'Auth\LoginController@logout')->name('AdminLogout');

        // product Crud
        Route::resource('product','ProductController');
        Route::post('product','ProductController@search')->name('product.search');
        Route::post('productstore','ProductController@store')->name('product.store');   
    });
});
