<hr>
<footer class="container-fluid text-center">
  <p>Footer Text</p>
</footer>

@yield('js')
<script>
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $(document).ready(function(){
        setTimeout(function(){$('.alertdisapper').fadeOut();}, 4000);
  });

  var url = $('meta[name="baseUrl"]').attr('content');

    $(document).on("click",'.btnDelete',function(event) {
      var url = $(this).data('url');
      event.preventDefault();
      if (confirm('Are you sure?')) {
              form = $('#deleteForm');
              form.attr('action', url);
              form.submit();
        }
    });

</script>