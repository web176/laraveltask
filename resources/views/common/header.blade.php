<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="{{ route('product.index') }}">Logo</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="{{ request()->is('product*') ? 'active' : '' }}"><a href="{{ route('product.index') }}">Product</a></li>
        <li><a href="{{ route('product.index') }}">Product</a></li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{ route('AdminLogout') }}"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
      </ul>
    </div>
  </div>
</nav>