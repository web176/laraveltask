@extends('layouts.index')

@section('title') Product List

@endsection

@section('content')

<div class="container-fluid text-center">  
<div class="row">
    <a href="{{ route('product.create') }}" ><button class="btn btn-primary">Create Product</button></a>
</div>  
  <div class="row content">
    <div class="col-sm-12 text-left"> 
    @include('common.flash')
      <table id="products" class="table table-striped table-bordered nowrap dataTable dt-responsive" style="width:100%">
          <thead>
            <tr>
                <th width="100px">No</th>
                <th>Product Name</th>
                <th>Image</th>
                <th>description</th>
                <th width="100px">Action</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
    </div>
  </div>
</div>
@include('common.forms')
@endsection

@section('js')

<script>

    var table;
    $(function() {
        table = $('#products').DataTable({
            processing: true,
            serverSide: true,
            "ajax": {
                "url":'{{ url(route("product.search"))  }}',
                "type": "POST",
                "async": false,
            },
            columns: [
                { data: 'sr_no' ,name:'sr_no', orderable:false },
                { data: 'product_name', name: 'product_name'},
                { data: 'image', name: 'image'},
                { data: 'description', name: 'description'},
                { data: 'action', name: 'action', orderable: false },
            ],
            "aaSorting": [[1,'desc']],
         });
    });

</script>
@endsection