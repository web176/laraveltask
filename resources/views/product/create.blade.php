@extends('layouts.index')

@section('title') {{ empty($product) ? ' product' : ' product' }} @endsection

@section('content')

<div class="container-fluid text-center">  
    <div class="row">
        <h2> Create product</h2>
    </div>   
    <div class="row content">
        <div class="col-sm-12 text-left"> 
    
            @if(empty($product))
                <form class="form" name="productstore" id="productstore" method="post" enctype="multipart/form-data" action="{{ route('product.store') }}">
            @else
                <form class="form" name="productupdate" id="productupdate" method="post" enctype="multipart/form-data" action="{{ route('product.update', ['product' => $product->id]) }}">
                    @method('PUT')
            @endif

            {{ csrf_field() }}

            <input type="hidden" name="id" id="id" value="{{ $product->id ?? '' }}" />
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label for="product_name" class="block">Product Name *</label>
                            </div>
                            <div class="col-sm-12">
                               
                                <input id="product_name" type="text" class="form-control @error('product_name') is-invalid @enderror" name="product_name" value="{{ $product->product_name ?? '' }}" autocomplete="product_name" autofocus>

                                @error('product_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong style="color: red;">{{ $message }}</strong>
                                    </span>
                                @enderror
                            
                            </div>
                        </div>      
                    </div>

                    <div class="col-md-6">
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label for="image" class="col-2 col-form-label">Image<span class="text-danger"></span> </label>
                            </div>
                            <div class="col-sm-12">
                                <input type="file" id="image" name="image" class="dropify" tabindex="3" data-show-remove="false" accept="image/x-png,image/jpg,image/jpeg"  data-allowed-file-extensions="jpg png jpeg " data-default-file="{{ !empty($product->image) ? url('images/image/'.$product->image) : ''}}"/>
                            @if(!empty($product->image))
                            <br>
                            <img class="card-img-top" src="{{url('uploads/'.$product->image)}}" alt="{{$product->image}}" height="50px" width="50px">
                            @endif

                            @error('image')
                                <span class="invalid-feedback" role="alert">
                                    <strong style="color: red;">{{ $message }}</strong>
                                </span>
                            @enderror

                            </div>
                        </div>      
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label for="description" class="col-2 col-form-label">Description<span class="text-danger"></span> </label>
                            </div>
                            <div class="col-sm-12">
                                <input id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ $product->description ?? '' }}" autocomplete="description" autofocus>
                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong style="color: red;">{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>      
                    </div>

                    <div class="col-md-6">
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label for="category" class="col-2 col-form-label">Category<span class="text-danger"></span> </label>
                            </div>
                            <div class="col-sm-12">
                               <select class="form-control" name="category">
                                @if(isset($categories))
                                    @foreach($categories as $category)
                                        <option class="form-control" @if(isset($product->category)) {{ $category->id == $product->category ? 'selected' : '' }} @endif  value="{{ $category->id  }}"> {{ $category->category_name  }}</option>
                                    @endforeach
                                @endif
                               </select>

                               @error('category')
                                    <span class="invalid-feedback" role="alert">
                                        <strong style="color: red;">{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>      
                    </div>

                </div>

                <button type="submit" class="btn btn-info"> {{ empty($product) ? 'Create' : 'Update' }}</button>
            </form>
            </div>
    </div>
</div>
@endsection

@section('js')

@endsection

