<!DOCTYPE html>

<html lang="en">

    <head>

        @include('common.head')

    </head>

        <body>


            @include('common.header')


            @yield('content')


            @include('common.footer')


        </body>

</html>