<?php 

return [
        'product' => [
            'create' => [
                        'success' => 'Product created success',
                        'error' => 'Product not create, something wrong',
            ],
            'update' => [
                'success' => 'Product updated success',
                'error' => 'Product not update, something wrong',
            ],
            'delete' => [
                'success' => 'Product deleted success',
                'error' => 'Product not delete, something wrong',
            ],
        ]
];