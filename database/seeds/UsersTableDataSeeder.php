<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([

            'name' => 'Admin User',

            'email' => 'admin@system.com',

            'password' => bcrypt('demo@1234'),

        ]);
    }
}
