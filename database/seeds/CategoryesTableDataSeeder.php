<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoryesTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 15; $i++) { 
            Category::create([

                'category_name' => 'Category '.$i

            ]);
        }
    }
}
