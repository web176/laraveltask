<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('product.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('product.create',['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['image'          => 'required|image|dimensions:min_width=700,min_height=700',
                                    'product_name'  => 'required',
                                    'description'   => 'required',
                                    'category'      => 'required']
                                );

        $product = new Product();
        $product->fill($request->all());

        if($request->file('image'))
        {
            $image = $request->file('image');
            $extension = $image->getClientOriginalExtension();
            Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));

            $product->image = $image->getFilename().'.'.$extension;      
        }


        if ($product->save()) {

            return redirect()->route('product.index')->with('success', trans('messages.product.create.success'));
        }
        return redirect()->route('product.index')->with('error', trans('messages.product.create.error'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::all();
        return view('product.create',['product' => $product,'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $this->validate($request, ['image'          => 'image|dimensions:min_width=100,min_height=100',
                                    'product_name'  => 'required',
                                    'description'   => 'required',
                                    'category'      => 'required']
                                );
                                
        $product->fill($request->all());

        if($request->file('image'))
        {
            $productimage = $request->file('image');
            $extension = $productimage->getClientOriginalExtension();
            Storage::disk('public')->put($productimage->getFilename().'.'.$extension,  File::get($productimage));

            $product->image = $productimage->getFilename().'.'.$extension;      
        }

        if($product->save())
        {
            return redirect()->route('product.index')->with('success',trans('messages.product.update.success'));
        }

        return redirect()->route('product.index')->with('error',trans('messages.product.update.error'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        if($product->delete())
        {
            return redirect(route('product.index'))->with('success', trans('messages.product.delete.success'));
        }
        
        return redirect(route('product.index'))->with('error', trans('messages.product.delete.error'));
    }

    /**
     * Search products.
     *
     * @param Request $request
     *
     * @return json
     */
    public function search(Request $request)
    {
        if ($request->ajax()) {

            $currentPage = ($request->start == 0) ? 1 : (($request->start / $request->length) + 1);

            Paginator::currentPageResolver(function () use ($currentPage) {
                return $currentPage;
            });
       
            $startNo = ($request->start == 0) ? 1 : (($request->length) * ($currentPage - 1)) + 1;
            $query = Product::selectRaw('products.*')->orderBy('id','DESC');

            $orderDir = $request->order[0]['dir'];
            $orderColumnId = $request->order[0]['column'];
            $orderColumn = str_replace('"', '', $request->columns[$orderColumnId]['name']);

            $query->where(function ($query) use ($request) {

                $query->orWhere('products.product_name', 'like', '%' . $request->search['value'] . '%');
            });

            $product = $query->orderBy($orderColumn, $orderDir)
                ->paginate($request->length)->toArray();

            $product['recordsFiltered'] = $product['recordsTotal'] = $product['total'];

            foreach ($product['data'] as $key => $products) {

                $params = [
                    'product' => $products['id'],
                ];

                $product['data'][$key]['sr_no'] = $startNo + $key;

                $editRoute = route('product.edit', $params);

                $deleteRoute = route('product.destroy', $params);

                $product['data'][$key]['action'] = '<a href="' . $editRoute .'"><button class="btn btn-primary">Edit</button></a>&nbsp&nbsp';
                $product['data'][$key]['action'] .= '<a href="javascript:void(0);" data-url="' . $deleteRoute . '" class="btnDelete"><button class="btn btn-danger">Delete</button></a>&nbsp&nbsp';
                $product['data'][$key]['image']  = '<img src="'.url('uploads/'.$products['image']).'" height="50px" width="50px">';
            }

            return response()->json($product);
        }
    }
}
